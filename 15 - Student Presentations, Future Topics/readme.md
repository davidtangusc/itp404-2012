##Topics beyond this class

###Server-side Frameworks
* ORM (Object Relational Mapping) Frameworks
* Unit Testing Frameworks like PHPUnit
* Node.js and how it differs from other server-side languages like PHP, Node frameworks like Express 
* RESTful Architectures and API design

###JavaScript
* Publish/Subscribe pattern (Pubsub)
* JavaScript Architecture & Client-side MVC Frameworks (Backbone.js, Angular, Knockout, Ember.js)
* Script loaders (Require.js) for modular JavaScript
* JavaScript module patterns & closures